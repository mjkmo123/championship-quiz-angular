import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-quiz-card',
  templateUrl: './quiz-card.component.html',
  styleUrls: ['./quiz-card.component.scss']
})
export class QuizCardComponent implements OnInit {
  @Input() data;
  @Output() selected = new EventEmitter<boolean>();
  @Input() button = true;
  constructor() { }

  ngOnInit(): void {
  }
  select(){
    this.selected.emit(true);
  }
}
