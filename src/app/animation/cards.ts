import {animate, state, style, transition, trigger} from "@angular/animations";

export const cards = [
  trigger('card', [
    state('ok', style({
      opacity: '100%',
      transform: 'scale(1)'
    })),
    state('fade', style({
      opacity: '30%',
      transform: 'scale(0)'
    })),
    transition('ok => fade', [
      animate('0.15s ease-in-out')
    ]),
    transition('fade => ok', [
      animate('0.75s ease-in-out')
    ])
  ]),
  trigger('winner', [
    state('show', style({
      opacity: '100%',
      transform: 'scale(1) rotate(720deg)'
    })),
    state('hide', style({
      opacity: '30%',
      transform: 'scale(0) rotate(0deg)',
    })),
    transition('show => hide', [
      animate('0.15s ease-in-out')
    ]),
    transition('hide => show', [
      animate('2s ease-in-out')
    ])
  ])
]
