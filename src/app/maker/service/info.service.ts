import { Injectable } from '@angular/core';
import {CrudService} from "../../auth/crud.service";
import {HttpClient} from "@angular/common/http";
import {AuthService} from "../../auth/auth.service";

@Injectable({
  providedIn: 'root'
})
export class InfoService extends CrudService{

  constructor(
    public httpClient: HttpClient,
    public authService: AuthService
  ) {
    super(authService, httpClient, '/api/information');
  }
}
