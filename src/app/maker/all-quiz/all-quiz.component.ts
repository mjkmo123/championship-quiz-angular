import { Component, OnInit } from '@angular/core';
import {Quiz} from "../../interfaces/Quiz";
import {QuizService} from "../service/quiz.service";
import {MatDialog} from "@angular/material/dialog";
import {AddEditQuizComponent} from "../add-edit-quiz/add-edit-quiz.component";
import {Clipboard} from "@angular/cdk/clipboard";
import {MatSnackBar} from "@angular/material/snack-bar";

@Component({
  selector: 'app-all-quiz',
  templateUrl: './all-quiz.component.html',
  styleUrls: ['./all-quiz.component.scss']
})
export class AllQuizComponent implements OnInit {
  quizzes: Quiz[];
  loading = true;
  constructor(private quizService: QuizService, private dialog: MatDialog, private clipboard: Clipboard, private snackBar: MatSnackBar) { }

  ngOnInit(): void {
    this.load();
  }
  load(){
    console.log('loading data..');
    this.quizService.getAll().subscribe((data) => {
      this.quizzes = data['data'];
      this.loading = false;
    });
  }
  add(){
    let d = this.dialog.open(AddEditQuizComponent, {
      width: '500px',
      data: {type: 'create'}
    });
    d.afterClosed().subscribe(() => {
      this.load();
    })
  }
  edit(id){
    let d = this.dialog.open(AddEditQuizComponent, {
      width: '500px',
      data: {type: 'edit', id: id}
    });
    d.afterClosed().subscribe(() => {
      this.load();
    })
  }
  copy(link){
    link = "https://championship-quiz-123.web.app/quiz/" + link;
    this.clipboard.copy(link);
    this.snackBar.open('link copied', 'ok');
  }
}
