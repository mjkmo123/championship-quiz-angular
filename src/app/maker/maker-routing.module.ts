import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import {AllQuizComponent} from "./all-quiz/all-quiz.component";
import {ViewQuizComponent} from "./view-quiz/view-quiz.component";

const routes: Routes = [
  { path: '', component: AllQuizComponent },
  {path: 'quiz/:id', component: ViewQuizComponent}
  ];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MakerRoutingModule { }
