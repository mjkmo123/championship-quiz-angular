import {Component, Inject, OnInit} from '@angular/core';
import {QuizService} from "../service/quiz.service";
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";

@Component({
  selector: 'app-add-edit-quiz',
  templateUrl: './add-edit-quiz.component.html',
  styleUrls: ['./add-edit-quiz.component.scss']
})
export class AddEditQuizComponent implements OnInit {
  form: FormGroup;
  sending = false;
  constructor(
    private quizService: QuizService,
    public dialogRef: MatDialogRef<AddEditQuizComponent>,
    @Inject(MAT_DIALOG_DATA) public data,
    private fb: FormBuilder
  ) { }

  ngOnInit(): void {
    this.createForm();
    if (this.data['type'] === 'edit'){
      this.getData();
    }
  }
  createForm(): void{
    this.form = this.fb.group({
      name: ['', Validators.required],
      type: ['private', Validators.required],
      image: ['', Validators.required]
    });
  }
  get type(){
    return this.form.get('type');
  }
  get name(){
    return this.form.get('name');
  }
  get image(){
    return this.form.get('image');
  }
  getData(): void{
    this.sending = true;
    this.quizService.get(this.data['id']).subscribe((data) => {
      data = data['quiz'];
      this.form.setValue({
        name: data['name'],
        type: data['type'],
        image: data['image']
      });
      this.sending = false;
    });
  }
  save(): void{
    this.sending = true;
    if (this.data['type'] === 'edit'){
      this.quizService.update(this.form.value, this.data['id']).subscribe(() => {
        this.dialogRef.close();
      });
    }else {
      this.quizService.save(this.form.value).subscribe((data) => {
        this.sending = false;
        this.dialogRef.close();
      });
    }
  }
}
