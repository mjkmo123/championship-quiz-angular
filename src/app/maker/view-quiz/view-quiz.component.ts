import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Params} from "@angular/router";
import {QuizService} from "../service/quiz.service";
import {Information} from "../../interfaces/Information";
import {MatDialog} from "@angular/material/dialog";
import {AddInfoComponent} from "../add-info/add-info.component";
import {InfoService} from "../service/info.service";

@Component({
  selector: 'app-view-quiz',
  templateUrl: './view-quiz.component.html',
  styleUrls: ['./view-quiz.component.scss']
})
export class ViewQuizComponent implements OnInit {
  loading = true;
  id: number;
  infos: Information[];
  constructor(
    private activatedRoute: ActivatedRoute,
    private quizService: QuizService,
    private dialog: MatDialog,
    private infoService: InfoService
  ) { }

  ngOnInit(): void {
    this.activatedRoute.params.subscribe((param: Params) => {
      this.id = param['id'];
    });
    this.load();
  }
  load(){
    this.quizService.get(this.id).subscribe((data) => {
      this.infos = data['info'];
      this.loading = false;
    });
  }
  add(){
    let d = this.dialog.open(AddInfoComponent, {
      width: '500px',
      data: {id: this.id}
    });
    d.afterClosed().subscribe(() => {
      this.load();
    });
  }
  remove(id){
    this.infoService.destroy(id).subscribe(() => {
      this.load();
    });
  }
}
