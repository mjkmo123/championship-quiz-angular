import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MakerRoutingModule } from './maker-routing.module';
import { AllQuizComponent } from './all-quiz/all-quiz.component';
import {MatProgressBarModule} from "@angular/material/progress-bar";
import {MatIconModule} from "@angular/material/icon";
import {MatButtonModule} from "@angular/material/button";
import {MatCardModule} from "@angular/material/card";
import { AddEditQuizComponent } from './add-edit-quiz/add-edit-quiz.component';
import {MatDialogModule} from "@angular/material/dialog";
import {ReactiveFormsModule} from "@angular/forms";
import {MatFormFieldModule} from "@angular/material/form-field";
import {MatInputModule} from "@angular/material/input";
import {MatSelectModule} from "@angular/material/select";
import {MatDividerModule} from "@angular/material/divider";
import { ViewQuizComponent } from './view-quiz/view-quiz.component';
import { AddInfoComponent } from './add-info/add-info.component';
import {MatSnackBarModule} from "@angular/material/snack-bar";


@NgModule({
  declarations: [AllQuizComponent, AddEditQuizComponent, ViewQuizComponent, AddInfoComponent],
  imports: [
    CommonModule,
    MakerRoutingModule,
    MatProgressBarModule,
    MatIconModule,
    MatButtonModule,
    MatCardModule,
    MatDialogModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    MatDividerModule,
    MatSnackBarModule
  ]
})
export class MakerModule { }
