import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {InfoService} from "../service/info.service";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";

@Component({
  selector: 'app-add-info',
  templateUrl: './add-info.component.html',
  styleUrls: ['./add-info.component.scss']
})
export class AddInfoComponent implements OnInit {
  form: FormGroup;
  sending = false;
  constructor(
    public dialogRef: MatDialogRef<AddInfoComponent>,
    private infoService: InfoService,
    private fb: FormBuilder,
    @Inject(MAT_DIALOG_DATA) public data
  ) { }

  ngOnInit(): void {
    this.form = this.fb.group({
      name: ['', Validators.required],
      picture_url: ['', Validators.required],
      quiz: this.data['id']
    })
  }
  get image(){
    return this.form.get('picture_url');
  }
  get name(){
    return this.form.get('name');
  }
  save(){
    this.sending = true;
    this.infoService.save(this.form.value).subscribe(() => {
      this.dialogRef.close();
    })
  }
}
