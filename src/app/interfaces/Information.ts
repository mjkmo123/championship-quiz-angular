export interface Information {
  id: number;
  quiz_id: number;
  picture_url: string;
  name: string;
}
