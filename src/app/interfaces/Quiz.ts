export interface Quiz {
  id: number;
  name: string;
  user_id: number;
  slug: string;
  type: string;
  image: string;
}
