import { Component, OnInit } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {AuthService} from "../auth/auth.service";
import {MatDialog} from "@angular/material/dialog";
import {RegisterComponent} from "../auth/register/register.component";

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  loading = true;
  data = [];
  constructor(
    private httpClient: HttpClient,
    public authService: AuthService,
    private dialog: MatDialog
  ) { }

  ngOnInit(): void {
    this.load();
  }
  load(){
    this.httpClient.get(this.authService.address + '/api/quiz/explore').subscribe((data) => {
      this.data = data['data'];
      console.log(data);
      this.loading = false;
    });
  }
  register(){
    this.dialog.open(RegisterComponent, {
      width: '300px',
      disableClose: true
    });
  }
}
