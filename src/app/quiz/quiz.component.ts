import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Params} from "@angular/router";
import {HttpClient} from "@angular/common/http";
import {AuthService} from "../auth/auth.service";
import {Information} from "../interfaces/Information";
import {cards} from "../animation/cards";

@Component({
  selector: 'app-quiz',
  templateUrl: './quiz.component.html',
  styleUrls: ['./quiz.component.scss'],
  animations: [
    cards
  ]
})
export class QuizComponent implements OnInit {
  slug = '';
  loading = true;
  data: Information[];
  card1 = 0;
  card2 = 1;
  card1selected = false;
  card2selected = false;
  selected = {
    stat: false,
    index: 0
  };
  constructor(
    private activatedRoute: ActivatedRoute,
    private httpClient: HttpClient,
    private authService: AuthService
  ) { }

  ngOnInit(): void {
    this.activatedRoute.params.subscribe((param: Params) => {
      this.slug = param['slug'];
    });
    this.load();
  }
  load(){
    this.httpClient.get(this.authService.address + '/api/quiz/start/' + this.slug).subscribe((data) => {
      this.data = data['data'];
      this.loading = false
    });
  }
  card1select(){
    if (this.data.length <= this.card1 + 1 || this.data.length <= this.card2 + 1){
      this.selected.stat = true;
      this.selected.index = this.card1;
    }else {
      (async () => {
        // Do something before delay
        this.card1selected = true;

        await this.delay(500);

        // Do something after
        if (this.card1 > this.card2){
          this.card2 = this.card1 + 1;
        }else {
          this.card2++;
        }
        this.card1selected = false;
      })();
    }
  }
  card2select(){
    if (this.data.length <= this.card1 + 1 || this.data.length <= this.card2 + 1){
      this.selected.stat = true;
      this.selected.index = this.card2;
    } else {
      (async () => {
        // Do something before delay
        this.card2selected = true;

        await this.delay(500);

        // Do something after
        if (this.card1 < this.card2){
          this.card1 = this.card2 + 1;
        }else {
          this.card1++;
        }
        this.card2selected = false;
      })();
    }
  }
  delay(ms: number) {
    return new Promise( resolve => setTimeout(resolve, ms) );
  }
}
