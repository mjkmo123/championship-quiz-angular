import { Injectable } from '@angular/core';
import {Observable} from "rxjs";
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Router} from "@angular/router";

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  address = 'https://championship.larsampad.ir'
  constructor(private httpClient: HttpClient, private router: Router) { }
  public isLogin(): boolean{
    return !!localStorage.getItem('isLogin')
      && !!localStorage.getItem('access_token');
  }
  public login(data): Observable<any>{
    return this.httpClient.post(this.address + '/api/login', data);
  }
  public register(data): Observable<any>{
    return this.httpClient.post(this.address + '/api/register', data);
  }
  public saveToken(token){
    localStorage.setItem('isLogin', '1');
    localStorage.setItem('access_token', token);
  }
  public token(): string{
    return localStorage.getItem('access_token');
  }
  public header(): HttpHeaders{
    return new HttpHeaders({
      Accept: 'application/json',
      Authorization: 'Bearer ' + this.token()
    });
  }
  public getUserInfo(): Observable<any>{
    return this.httpClient.get(this.address + '/api/user', {
      headers: this.header()
    })
  }
  public logout(): void{
    localStorage.clear();
    this.router.navigate(['']);
  }
}
