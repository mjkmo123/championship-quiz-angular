import { Component, OnInit } from '@angular/core';
import {AuthService} from "../auth.service";
import {MatDialogRef} from "@angular/material/dialog";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  form: FormGroup;
  logging = false;
  constructor(
    private authService: AuthService,
    public dialogRef: MatDialogRef<LoginComponent>,
    private fb: FormBuilder
  ) { }

  ngOnInit(): void {
    this.form = this.fb.group({
      email: ['', Validators.required],
      password: ['', Validators.required]
    })
  }
  login(){
    this.logging = true;
    this.authService.login(this.form.value).subscribe((data) => {
      this.authService.saveToken(data['token']['accessToken']);
      this.logging = false;
      this.dialogRef.close();
    },
      (error) => {
      this.logging = false;
      })
  }
}
