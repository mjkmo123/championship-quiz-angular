import { Component, OnInit } from '@angular/core';
import {AuthService} from "../auth.service";
import {MatDialogRef} from "@angular/material/dialog";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {HttpErrorResponse} from "@angular/common/http";

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {
  form: FormGroup;
  registering = false;
  email = false;
  constructor(
    private authService: AuthService,
    public dialogRef: MatDialogRef<RegisterComponent>,
    private fb: FormBuilder
  ) { }

  ngOnInit(): void {
    this.form = this.fb.group({
      email: ['',[Validators.required]],
      password: ['',[Validators.required, Validators.minLength(8)]],
      name: ['',[Validators.required]]
    })
  }
  get password(){
    return this.form.get('password');
  }
  register(){
    this.registering = true;
    this.authService.register(this.form.value).subscribe((data) => {
      this.authService.saveToken(data['token']['accessToken']);
      this.registering = false
      this.dialogRef.close();
    }, (err: HttpErrorResponse) => {
      this.registering = false
      if (+err.status === 422){
        this.email = true;
      }
    })
  }
}
