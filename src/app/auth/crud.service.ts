import { Injectable } from '@angular/core';
import {AuthService} from './auth.service';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {catchError, retry} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class CrudService {
  private prefix: string;
  constructor(
    public authService: AuthService,
    public httpClient: HttpClient,
    prefix: string
  ) {
    this.prefix = prefix;
  }
  // ---------------
  public getAll(): Observable<any>{
    return this.httpClient.get(this.authService.address + this.prefix, {
      headers: this.authService.header()
    }).pipe(
      retry(3),
    );
  }
  // ---------------
  public save(data: object): Observable<any>{
    return this.httpClient.post(this.authService.address + this.prefix, data, {
      headers: this.authService.header()
    }).pipe(
      retry(3),
    );
  }
  // ---------------
  public get(id: number): Observable<any>{
    return this.httpClient.get(this.authService.address + this.prefix + '/' + id, {
      headers: this.authService.header()
    }).pipe(
      retry(3),
    );
  }
  // ---------------
  public update(data: object, id: number): Observable<any>{
    return this.httpClient.put(this.authService.address + this.prefix + '/' + id, data, {
      headers: this.authService.header()
    }).pipe(
      retry(3),
    );
  }
  // ---------------
  public destroy(id: number): Observable<any>{
    return this.httpClient.delete(this.authService.address + this.prefix + '/' + id, {
      headers: this.authService.header()
    }).pipe(
      retry(3),
    );
  }
}
