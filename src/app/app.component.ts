import { Component } from '@angular/core';
import {AuthService} from "./auth/auth.service";
import {MatDialog} from "@angular/material/dialog";
import {LoginComponent} from "./auth/login/login.component";
import {RegisterComponent} from "./auth/register/register.component";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'championship-quiz';
  constructor(public authService: AuthService, private dialog: MatDialog) {
  }
  login(){
    this.dialog.open(LoginComponent, {
      width: '300px',
      disableClose: true
    });
  }
  register(){
    this.dialog.open(RegisterComponent, {
      width: '300px',
      disableClose: true
    });
  }
}
